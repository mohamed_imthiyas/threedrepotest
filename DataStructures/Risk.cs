﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;

namespace ListExample.DataStructures
{
    [Serializable]
    public class Risk : INotifyPropertyChanged
    {
        public string _id { get; set; } = null;
        public string owner { get; set; } = null;
        public Int64 created { get; set; } = 0;
        public string thumbnail { get; set; } = null;
        public string name { get; set; } = "";
        public string desc { get; set; } = "";

        public string mitigation_status  { get; set; } = "";
        public Int32 overall_level_of_risk { get; set; } = -1;
        public string category = "";

        //UI Model Property
        private BitmapImage thumbnailImage;
        public BitmapImage ThumbnailImage
        {
            get => thumbnailImage;
            set
            {
                thumbnailImage = value;
                NotifyPropertyChanged(nameof(ThumbnailImage));
            }
        }
        public int number { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }

    [Serializable]
    public class RiskList
    {
        public List<Risk> risks = null;
    }
}