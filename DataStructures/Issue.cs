﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ListExample.DataStructures
{
    [Serializable]
    public class IssuesList
    {
        public List<Issue> issues = null;
    }

    [Serializable]
    public class Issue : INotifyPropertyChanged
    {
        public string _id { get; set; } = "";
        public string name { get; set; } = "";
        public string status { get; set; } = "";
        public string owner { get; set; } = "";
        public Int64 created { get; set; } = 0;
        public Int64 due_date { get; set; } = 0;
        public int number { get; set; } = -1;
        public string topic_type { get; set; } = "";
        public string priority { get; set; } = "";
        public string desc { get; set; } = "";
        public string thumbnail { get; set; }

        //UI Model Property
        private BitmapImage thumbnailImage;
        public BitmapImage ThumbnailImage
        {
            get => thumbnailImage;
            set
            {
                thumbnailImage = value;
                NotifyPropertyChanged(nameof(ThumbnailImage));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}