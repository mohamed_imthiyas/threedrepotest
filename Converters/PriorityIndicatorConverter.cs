﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Windows.Data;
using System.Windows.Media;

namespace ListExample.Converters
{
    public class PriorityIndicatorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var pInd = $"{parameter}PriorityIndicator:{value}";
            var rm = new ResourceManager("ListExample.Properties.Resources", Assembly.GetExecutingAssembly());
            var color = (Color)ColorConverter.ConvertFromString(rm.GetString(pInd));
            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
