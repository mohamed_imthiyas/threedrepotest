﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Windows.Data;

namespace ListExample.Converters
{
    public class StatusIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var status = $"{parameter}StatusIcon:{value}";
            var rm = new ResourceManager("ListExample.Properties.Resources", Assembly.GetExecutingAssembly());
            var s = (char)(System.Convert.ToInt32(rm.GetString(status), 16));
            return s;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
