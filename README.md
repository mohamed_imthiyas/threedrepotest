# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How is it done? ###

* MVVM Light Toolkit
* Seperate Views - Issues, Risks
* Fixed some variable into properties in the Models
* New converters are introduced
* New SearchBox style was created using Blend

### Whats not done? ###

* Status icon colour is now works only based on Priority colour/Level of Risk colour. It can be easily fixed by creating enums/converter. Due to time limitation I have not completed that.
* Issue & Risk models can both be derived from a base class to avoid repetitions of the properties.
* IssuesViewModel & RisksViewModel can also have a base class to avoid method repetitions.
* The ListView in both IssuesView & RisksView can have a common DataTemplate but it is debatable. 

