using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using ListExample.DataStructures;
using ListExample.Utility;
using System;
using System.Collections.Generic;

namespace ListExample.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private bool isIssuesVisible;
        private bool isRisksVisible;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            RefreshCommand = new RelayCommand(RelayCommandExecute);
            IsIssuesVisible = true;
        }

        private void Initiate()
        {
            throw new NotImplementedException();
        }

        private void RelayCommandExecute()
        {
            var token = IsIssuesVisible ? "issues" : "risks";
            Messenger.Default.Send<string>(SearchText, token);
        }

        public RelayCommand RefreshCommand { get; private set; }

        public string SearchText { get; set; }

        public bool IsIssuesVisible
        {
            get => isIssuesVisible;
            set
            {
                if (value)
                {
                    SearchText = String.Empty;
                    RaisePropertyChanged("SearchText");
                    Messenger.Default.Send<string>(null, "issues");
                }
                isIssuesVisible = value;
            }
        }

        public bool IsRisksVisible
        {
            get => isRisksVisible;
            set
            {
                if (value)
                {
                    SearchText = String.Empty;
                    RaisePropertyChanged("SearchText");
                    Messenger.Default.Send<string>(null, "risks");
                }
                isRisksVisible = value;
            }
        }
    }
}