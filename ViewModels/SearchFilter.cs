﻿namespace ListExample.ViewModels
{
    public class SearchFilter
    {
        public string SearchText { get; set; }

        public bool IsIssuesVisible { get; set; }
    }
}