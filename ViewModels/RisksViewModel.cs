﻿using GalaSoft.MvvmLight.Messaging;
using ListExample.DataStructures;
using ListExample.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ListExample.ViewModels
{
    public class RisksViewModel
    {
        public RisksViewModel()
        {
            Messenger.Default.Register<string>(this, "risks", GetRiskList);
            Risks = new BindingList<Risk>();
        }

        public BindingList<Risk> Risks { get; set; }

        public void GetRiskList(string filter)
        {
            Risks.Clear();
            var dataManager = new DataManager();
            var list = new List<Risk>();
            dataManager.GetRiskList((data, y) =>
            {
                App.Current.Dispatcher.Invoke(delegate
                {
                    if (!string.IsNullOrEmpty(filter))
                    {
                        filter = filter.Trim().ToLower();
                        data = data.Where(c => c.name.ToLower().Contains(filter) ||
                        c.owner.ToLower().Contains(filter) ||
                        c.desc.ToLower().Contains(filter)).ToList();
                    }
                    int i = 0;
                    foreach (var item in data.OrderBy(c => c.name))
                    {
                        if (item.thumbnail != null)
                        {
                            dataManager.GetBinaryStream(item.thumbnail, (s, e) => OnGetBinaryStream(s, item));
                        }
                        item.number = ++i;
                        Risks.Add(item);
                    }
                });
            });
        }

        private void OnGetBinaryStream(Stream stream, Risk risk)
        {
            App.Current.Dispatcher.Invoke(delegate
            {
                var image = new BitmapImage();
                using (var mem = new MemoryStream())
                {
                    stream.CopyTo(mem);
                    mem.Position = 0;
                    image.BeginInit();
                    image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = mem;
                    image.EndInit();
                }
                image.Freeze();
                risk.ThumbnailImage = image;
            });
        }

    }

}
