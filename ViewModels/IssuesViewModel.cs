﻿using GalaSoft.MvvmLight.Messaging;
using ListExample.DataStructures;
using ListExample.Utility;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;

namespace ListExample.ViewModels
{
    public class IssuesViewModel
    {
        public IssuesViewModel()
        {
            Messenger.Default.Register<string>(this, "issues", GetIssuesList);
            Issues = new BindingList<Issue>();
            GetIssuesList(null);
        }

        public BindingList<Issue> Issues { get; set; }

        public void GetIssuesList(string filter)
        {
            Issues.Clear();
            var dataManager = new DataManager();
            var list = new List<Issue>();
            dataManager.GetIssueList((data, y) =>
            {
                App.Current.Dispatcher.Invoke(delegate
                {
                    if (!string.IsNullOrEmpty(filter))
                    {
                        filter = filter.Trim().ToLower();
                        data = data.Where(c => c.number.ToString().Contains(filter) ||
                        c.name.ToLower().Contains(filter) ||
                        c.owner.ToLower().Contains(filter) ||
                        c.desc.ToLower().Contains(filter)).ToList();
                    }
                    foreach (var item in data.OrderBy(c => c.number))
                    {
                        if (item.thumbnail != null)
                        {
                            dataManager.GetBinaryStream(item.thumbnail, (s, e) => OnGetBinaryStream(s, item));
                        }
                        Issues.Add(item);
                    }
                });
            });
        }

        private void OnGetBinaryStream(Stream stream, Issue issue)
        {
            App.Current.Dispatcher.Invoke(delegate
            {
                var image = new BitmapImage();
                using (var mem = new MemoryStream())
                {
                    stream.CopyTo(mem);
                    mem.Position = 0;
                    image.BeginInit();
                    image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = mem;
                    image.EndInit();
                }
                image.Freeze();
                issue.ThumbnailImage = image;
            });
        }
    }
}
